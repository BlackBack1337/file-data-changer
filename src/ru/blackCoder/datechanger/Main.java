package ru.blackCoder.datechanger;

import java.io.*;
import java.util.Date;

public class Main
{
    private static String newDate;

    public static void main(String[] args)
    {
        newDate = getTextFromConfig();

        changeFileDate(new File("files"));
    }

    private static void changeFileDate(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                changeFileDate(fileEntry);
            } else {
                fileEntry.setLastModified(new Date(newDate).getTime());
            }
        }
    }

    private static String getTextFromConfig()
    {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("config.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
